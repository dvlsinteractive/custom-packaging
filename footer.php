	<div class="footerOffset"></div>
</div><!--end page-->

<footer>
	<div class="container">
		<a href="#_">
			<?php include('includes/logo.php'); ?>
		</a>
		<div class="text">
			<p>&copy; Copyright <?php echo date('Y'); ?> Custom Packaging, LP. - All Rights Reserved</p>
		</div>
	</div>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/jquery.2.1.4.min.js"><\/script>')</script>
<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/animate-it.js"></script>
<script type="text/javascript" src="js/functions.min.js"></script>


<script>
(function(){ 
  var heading = document.getElementById('pageTitle');
  if(heading != null){
    var pieces = heading.innerHTML.split('<br>');
    var dressed = pieces.reduce(function(a,b){
    return a + '<strong>' + b + '</strong>';
    });
    heading.innerHTML = dressed;
  }
})();
</script>

</body>
</html>