<?php 
	$subnav = false;
	$page = "news";
	$section = "news";
	
	include('header.php'); 
?>

<section class="news">
	<div class="container">
		<h1>News</h1>
		
		<div class="newsLinks">
			<div class="row">
				<div class="link-item">
					<p class="date">November 10, 2016</p>
					<h2><a href="#">Food Safety and RPC's</a></h2>
					<p class="desc">Study: Reusable Plastic Containers are Difficult to Clean</p>
				</div>
				
				<div class="link-item">
					<p class="date">June 10, 2015</p>
					<h2><a href="#">Volunteering for Habitat for Humanity</a></h2>
					<p class="desc">Volunteers for Habitat for Humanity</p>
				</div>
			</div>
			
			<div class="row">
				<div class="link-item">
					<p class="date">November 7, 2014</p>
					<h2><a href="#">Corrugated produce cartons are the cleaner option</a></h2>
					<p class="desc">For those in the fresh produce sector handling vegetables and fruit items, corrugated packaging is the safest solution.</p>
				</div>
				
				<div class="link-item">
					<p class="date">October 31, 2014</p>
					<h2><a href="#">RPC's fall short of safety standards</a></h2>
					<p class="desc">A new study demonstrates that Reusable Plastic Containers (RPCs) used to ship fruits and vegetables in Canada are not properly sanitized and show traces of E coli.</p>
				</div>
			</div>
			
			<div class="row">
				<div class="link-item">
					<p class="date">March 28, 2014</p>
					<h2><a href="#">Custom Packaging CEO Speaks at Fort Campbell</a></h2>
					<p class="desc">Jackie Cowden, CEO of Custom Packaging, Inc., speaks to guests at Cole Park Commons Tuesday afternoon during a Women’s History Month luncheon, hosted by Fort Campbell’s Equal Employment Opportunity Office.</p>
				</div>
				
				<div class="link-item">
					<p class="date">November 6, 2013</p>
					<h2><a href="#">Study Compares cost for corrugated vs RPC's for shipping tomatoes</a></h2>
					<p class="desc">Results of a detailed, activity-based accounting comparison show that shipping tomatoes in corrugated saves $6.8 million annually versus using RPCs.</p>
				</div>
			</div>
						
			
		</div><!--end newsLinks-->
		
	</div>
</section>

<div class="newsBackground" style="background:url('webimages/cp-exterior.jpg');"></div>

<?php include('footer.php'); ?>