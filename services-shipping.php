<?php 
	$subnav = true;
	$page = "manufacturing";
	$section = "services";
	
	include('header.php'); 
?>

<section class="fullWidth">
	<div class="container">
		<div class="fullImage" style="background:url('http://placehold.it/1400x550');">
		</div>
	</div>
</section>


<section class="pageTitle">
	<div class="container">
		<h1>Full-width Content</h1>
	</div>
</section>



<section>
	<div class="container">
		
		<div class="imageGallery popup-gallery">
			<a href="http://placehold.it/850x600?text=image+Lg">
				<span class="screen"></span>
				<img src="http://placehold.it/500x380/03A9F4/fff?text=image+1">
			</a>
			
			<a href="http://placehold.it/850x600?text=image+Lg">
				<span class="screen"></span>
				<img src="http://placehold.it/500x380/03A9F4/fff?text=image+2">
			</a>
			
			<a href="http://placehold.it/850x600?text=image+Lg">
				<span class="screen"></span>
				<img src="http://placehold.it/500x380/03A9F4/fff?text=image+3">
			</a>
			
			<a href="http://placehold.it/850x600?text=image+Lg">
				<span class="screen"></span>
				<img src="http://placehold.it/500x380/03A9F4/fff?text=image+4">
			</a>
			
			<a href="http://placehold.it/850x600?text=image+Lg">
				<span class="screen"></span>
				<img src="http://placehold.it/500x380/03A9F4/fff?text=image+5">
			</a>
			
			<a href="http://placehold.it/850x600?text=image+Lg">
				<span class="screen"></span>
				<img src="http://placehold.it/500x380/03A9F4/fff?text=image+6">
			</a>
		</div>
		
	</div>
</section>


<section class="twoColumnText content-last content-full">
	<div class="container">		
		<div class="content">			
			<p>The structural design of packaging must be both functional and economical.  Each member of Custom Packaging’s design team has on average 15 years of corrugated design experience. Our designers have backgrounds in P-O-P Displays as well as corrugated and industrial packaging.  Regardless of your industry, we can design and manufacture a custom solution to meet your needs.  Our design team has the creativity, knowledge and expertise to see your project through from concept to production. We work to maximize board efficiency and reduce waste and scrap in our design process. Custom Packaging is committed to sustainability, which begins with the design.</p>
			<p>Custom Packaging can produce renderings, as well as unprinted or full-color samples with our digital printing capability.</p>
			<p><a href="contact.php">Contact us</a> today to get an experienced and creative designer working on your project.</p>
		</div>
		
	</div>
</section>


<?php include('footer.php'); ?>