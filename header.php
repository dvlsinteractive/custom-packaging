<?php $host = $_SERVER['SERVER_NAME']; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="title" content="">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	<!--[if IE]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<title>Custom Packaging</title>

	<link rel="shortcut icon" href="favicon.ico">
	<link rel="apple-touch-icon" href="apple-touch-icon-114x114.png">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,600,700" rel="stylesheet" type="text/css"/>
	<link rel='stylesheet' href='css/layout.min.css' />
	
<?php if($host == "localhost") { ?>
<script src="http://localhost:35729/livereload.js?snipver=1"></script>

<style>
	body:before {
		background:#c0392b;
		display: block;
		color:#fff;
		padding:2%;
	}
</style>
<?php } ?>

</head>

<body<?php if($host == "localhost") { echo ' id="debug"'; } ?>>

<div class="page<?php if($page == "home") { echo " home"; }?>">

<header <?php if($subnav) { echo "class='hasSubnav'"; }?>>
	<div class="container">
		<nav>
			<a href="index.php" class="logo" title="Custom Packaging"><?php include('includes/logo.php'); ?></a>
			<ul class="primary">
				<li<?php if($section == "products"){echo " class='selected'";}?>><a href="products.php">Products</a></li>
				<li<?php if($section == "services"){echo " class='selected'";}?>><a href="services.php">Services</a></li>
				<li<?php if($section == "sustainability"){echo " class='selected'";}?>><a href="sustainability.php">Sustainability</a></li>
				<li<?php if($section == "about"){echo " class='selected'";}?>><a href="about.php">About Us</a></li>
				<li<?php if($section == "contact"){echo " class='selected'";}?>><a href="contact.php">Contact</a></li>
	    </ul>
	    
	    <ul class="secondary">
				<li><a href="news.php">News</a></li>
				<li class="social"><a href="#"><span class="icon-twitter"></span></a></li>
				<li class="social"><a href="#"><span class="icon-instagram"></span></a></li>
			</ul>
			
			<button class="menuLink hamburger hamburger--elastic" type="button">
			  <span class="hamburger-box">
			    <span class="hamburger-inner"></span>
			  </span>
			  <span class="label">
			  	<span>Menu</span>
			  </span>
			</button>
			
			<div class="offCanvas">
				<div class="inner">
					<ul>
						<li<?php if($section == "products"){echo " class='selected'";}?>><a href="products.php">Products</a></li>
						<li<?php if($section == "services"){echo " class='selected'";}?>><a href="services.php">Services</a>
							<ul>
								<li><a href="services-design.php">Design</a></li>
								<li><a href="services-manufacturing.php">Manufacturing</a></li>
								<li><a href="services-assembly-fulfillment.php">Assembly & Fulfillment</a></li>
								<li><a href="services-shipping.php">Shipping</a></li>
							</ul>
						</li>
						<li<?php if($section == "sustainability"){echo " class='selected'";}?>><a href="sustainability.php">Sustainability</a></li>
						<li<?php if($section == "about"){echo " class='selected'";}?>><a href="about.php">About Us</a>
							<ul>
								<li><a href="about-management.php">Our Management</a></li>
								<li><a href="about-iso9001.php">ISO9001</a></li>
								<li><a href="about-request-certificate.php">Request A Certificate Request A Certificate Request A Certificate</a></li>
								<li><a href="about-locations.php">Locations</a></li>
								<li><a href="about-careers.php">Career Opportunities</a></li>
							</ul>
						</li>
						<li<?php if($section == "contact"){echo " class='selected'";}?>><a href="contact.php">Contact</a></li>
						<li><a href="news.php">News</a></li>
						<li class="social"><a href="#"><span class="icon-twitter"></span></a></li>
						<li class="social"><a href="#"><span class="icon-instagram"></span></a></li>
					</ul>
				</div>
			</div>
		</nav>
		
		<?php if($subnav) { ?>
		<div class="subnav">
			<?php if($section == "about") { ?>
			<ul>
				<li><a href="about-management.php">Our Management</a></li>
				<li><a href="about-iso9001.php">ISO9001</a></li>
				<li><a href="about-request-certificate.php">Request A Certificate</a></li>
				<li><a href="about-locations.php">Locations</a></li>
				<li><a href="about-careers.php">Career Opportunities</a></li>
			</ul>
			<?php } ?>
			
			<?php if($section == "services") { ?>
			<ul>
				<li><a href="services-design.php">Design</a></li>
				<li><a href="services-manufacturing.php">Manufacturing</a></li>
				<li><a href="services-assembly-fulfillment.php">Assembly & Fulfillment</a></li>
				<li><a href="#_">Shipping</a></li>
			</ul>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</header>

<?php if($page != 'home') { ?>
<div class="headerOffset"></div>
<?php } ?>