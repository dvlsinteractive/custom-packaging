<?php 
	$subnav = true;
	$page = "services";
	$section = "services";
	
	include('header.php'); 
?>

<section class="pageIntroFull">
	<div class="container">
		<div class="image" style="background:url('https://unsplash.it/1200/750/?random');"></div>
		<div class="text">
			<h2>Our <strong>Services</strong></h2>
			<p>Custom Packaging is a full service manufacturer of corrugated shipping containers and Point of Purchase display solutions.</p>

			<p>We pride ourselves on our excellent customer service, short lead times and quality products. Our Customer Service department is professional, courteous and responsive to the needs of our customers.  As an independent sheet plant, we are small enough to respond and large enough to handle your requirements. With an ISO9001 registered quality system, we can manufacture quality products time after time.</p>

			<p><a href="#">Contact us</a> today and let one of our knowledgeable Sales Representatives show you how we can help get your product to market safely, efficiently, and sustainably.</p>
		</div>
	</div>
</section>

<section class="imageLinks">
	<div class="container animatedParent animateOnce" data-appear-top-offset="-250" data-sequence="500"> 
		<a href="services-design.php" class="imageLink animated fadeInLeft" data-id="1">
			<img src="https://unsplash.it/550/320/?random" />
			<span>Design</span>
		</a>
		
		<a href="#_" class="imageLink animated fadeInRight" data-id="1">
			<img src="http://placehold.it/550x320" />
			<span>Manufacturing</span>
		</a>
		
		<a href="#_" class="imageLink animated fadeInLeft" data-id="2">
			<img src="http://placehold.it/550x320" />
			<span>Assembly & Fulfillment</span>
		</a>
		
		<a href="#_" class="imageLink animated fadeInRight" data-id="2">
			<img src="http://placehold.it/550x320" />
			<span>Shipping</span>
		</a>
	</div>
</section>



<?php include('footer.php'); ?>