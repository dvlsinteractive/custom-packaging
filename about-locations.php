<?php 
	$subnav = true;
	$page = "locations";
	$section = "about";
	
	include('header.php'); 
?>

<section class="twoColumnText locations">
	<div class="container">
		<h1>Locations</h1>
		
		<div class="content">
			<div class="googleMap"></div>
		</div>
		
		<aside>
			<div class="locationLinks">
				<div class="location-item">
					<img src="webimages/location-lebanon.jpg" />
					<h3>Lebanon Facility</h3>
					<p>1315 W. Baddour Pkwy
					<br />Lebanon, TN 37087</p>
					<p><a href="#">615-444-6025</a></p>
					<p><a href="#">Get Directions</a></p>
				</div>
				
				<div class="location-item">
					<img src="webimages/location-arden.jpg" />
					<h3>Arden Facility</h3>
					<p>20 Beale Rd.
					<br />Arden, NC 28704</p>
					<p><a href="#">828-684-5060</a></p>
					<p><a href="#">Get Directions</a></p>
				</div>
			</div>
		</aside>
		
	</div>
</section>



<?php include('footer.php'); ?>