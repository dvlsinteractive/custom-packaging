<?php 
	$subnav = true;
	$page = "about";
	$section = "about";
	
	include('header.php'); 
?>

<section class="pageIntroFull">
	<div class="container">
		<div class="image" style="background:url('http://custompackaging.com/new/site/web/assets/2017/05/AboutUs.jpg');"></div>
		<div class="text">
			<h2>About <strong>Us</strong></h2>
			<p>Custom Packaging, Inc. is a family-owned and operated independent sheet plant.  Since 1968, we have been a leader in the manufacturing of corrugated cartons, graphics packaging and point-of-purchase displays.  With continual investment in our equipment, technology and employees, Custom Packaging has built a company with the ability and expertise to help you throughout the packaging process.  From concept to completion, we can provide valuable time-saving service to your company.</p>
			<p><a href="#">Contact us</a> today to learn more about Custom Packaging and how we can help with your packaging needs.</p>
		</div>
	</div>
</section>

<section class="splitText">
	<div class="container">
		<h2>Our <strong>History</strong></h2>
		<div class="column">
			<p>Custom Packaging, LP. was founded by (Walter) Lawrence West, the late Robert Werckle and the late Dick Lindsey. These gentlemen were former employees of Container Corporation of America, located in Nashville, TN. Each had many years of experience in the industry, and their diversified areas of expertise enabled them to successfully start and rapidly grow the company. Mr. Lindsey’s background was in Administration, Mr. Werckle's in Sales, and Mr. West's in Production. Since 1988 Custom Packaging has been a family owned and operated business.</p>

			<p>The original facility in Lebanon, Tennessee, has expanded 7 times to its current size of 210,000 square feet, employing over 120 people. In 1988, the company diversified into corrugated graphics. The operation now includes designing and manufacturing of point-of-purchase displays, graphics packaging, and industrial packaging. Since inception, we have continued to upgrade existing machinery as well as invest in new machinery, keeping Custom Packaging on the forefront of manufacturing technology.</p>

			<p>In 1970, an additional facility was added in Arden, North Carolina, to complement the customer base in East Tennessee, beginning with a focus on shipping cartons and industrial packaging. Today, that facility is incorporating point-of-purchase displays and graphics packaging into its mix and has expanded to 150,000 square feet with an employee base of more than 50 people.</p>
		</div>

		<div class="column">
			<p>Throughout Custom Packaging’s history, we have focused on continual growth through innovation. Investments in technology, software, machinery, and training of our entire workforce ensures we are able to deliver the best quality product at a competitive price.</p>

			<p>Both the Tennessee and North Carolina facilities have installed solar arrays. In Tennessee, a 100kw system produces 10% of the electric cost of operating the facility. North Carolina has installed a 200kw system, which has been beneficial in helping offset the rise in electrical costs.</p>

			<p><a href="#_">Contact us</a> today and let one of our knowledgeable Sales Representatives introduce our company to your company.</p>
			
			<a href="#_" class="imageLink">
			<img src="http://placehold.it/550x320" />
			<span>Shipping</span>
		</a>
		</div>
	</div>
</section>



<?php include('footer.php'); ?>