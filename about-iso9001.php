<?php 
	$subnav = true;
	$page = "ISO9001";
	$section = "about";
	
	include('header.php'); 
?>

<section class="twoColumnText content-last">
	<div class="container">
		
		<div class="column-narrow">
			<h1>ISO9001</h1>
			<p>Quality is our culture at Custom Packaging. Custom Packaging’s entire operation is based on a tripod of 3 fundamental principles. These principles are Safety, Quality, and Productivity.</p>
			<p>These three principles are embodied in our Quality Policy.</p>
		</div>
		
		<div class="column-wide">
			<h2>Quality Policy:</h2>
			<p>Custom Packaging is committed to exceeding customer expectations in the design and manufacture of printed corrugated shipping containers and high graphics corrugated displays; and manufacture of other corrugated materials such as corrugated pallets, partitions, and pads.</p>
			<ul>
				<li>We achieve our goals by complying with customer and other requirements, working safely, sharing ideas, producing quality products and services, and continually improving our quality impact.</li>
				<li>Quality objectives are established and reviewed during Management Review.</li>
				<li>This policy is communicated through training.</li>
				<li>This policy is reviewed and amended through Management Review.</li>
			</ul>
			<p>This policy is the motivating factor for everything we do. At Custom Packaging, exceeding Customer expectations is not just a statement, it is a way of life. It begins with our Executive Management and flows throughout the entire organization. Each step of the process from Sales and Customer Service until the product has been delivered is driven by a desire to meet product requirements and excel in our service to the Customer. Call any member of our staff and let us put this mindset to work for you.</p>	
		</div>
		
	</div>
</section>




<?php include('footer.php'); ?>