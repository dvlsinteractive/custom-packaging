<?php 
	$subnav = true;
	$page = "request";
	$section = "about";
	
	include('header.php'); 
?>

<section>
	<div class="container">
		<h1>Request <strong>A Certificate</strong></h1>
		
		<form>
			<span class="half">
				<label>Name:</label>
				<input type="text" />
			</span>
			
			<span class="half">
				<label>Company Name:</label>
				<input type="text" />
			</span>
			
			<span class="half">
				<label>Email Address:</label>
				<input type="text" />
			</span>
			
			<span class="half">
				<label>Phone Number:</label>
				<input type="text" />
			</span>
			
			<span class="full">
				<label>Certificate Type:</label>
				<input type="checkbox" />
			</span>
			
			<span class="full">
				<label>Additional Requests:</label>
				<textarea></textarea>
			</span>
			
			<span class="full">
				<button type="submit">Submit</button>
			</span>
			
		</form>
	</div>
</section>




<?php include('footer.php'); ?>