// load Gulp dependencies
var gulp = require('gulp'); 
var sass = require('gulp-ruby-sass');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var combineMq = require('gulp-combine-mq');
var uglify = require('gulp-uglify');
var livereload = require('gulp-livereload');


function onError(err) {
    console.log(err);
}


// watch SCSS
gulp.task('styles', function () {

  // compile scss
	return sass('sass/main.scss')
    .on('error', function (err) {
        console.error('Error!', err.message);
    })
    
    // automatically add needed browser prefixing
    .pipe(autoprefixer({
        browsers: ['> 5%', 'IE 9','last 2 versions'],
        cascade: false,
        remove: true
    }))
    
    // combine media queries & move to end of document
    .pipe(combineMq({
        beautify: false
    }))
		    
    // minify/compress css
    .pipe(cleanCSS({
	    keepBreaks: false,
	    processImport: true
	  }))
	  
	  // rename in folder
		.pipe(rename('layout.min.css'))			
    .pipe(gulp.dest('../css'))
    
    // re-inject styles into page
    .pipe(livereload());
});



// watch javascript
gulp.task('scripts', function() {
    return gulp.src('../js/functions.js')
    	.pipe(uglify())
    	.pipe(rename('functions.min.js'))			
			.pipe(gulp.dest('../js'))
      .pipe(livereload());
});




// watch files for changes
gulp.task('watch', function() {	
	livereload.listen();

  //watch and reload PHP and html
  gulp.watch('../**/*.php').on('change', function(file) {
  	livereload.changed(file.path); 
  });
  
  gulp.watch('../**/*.html').on('change', function(file) {
  	livereload.changed(file.path);
 	});

  gulp.watch('../js/*.js', ['scripts']);
  gulp.watch('sass/*.scss', ['styles']);
  gulp.watch('sass/**/*.scss', ['styles']);
});



// Ready? Set... Go!
gulp.task('default', ['styles', 'scripts', 'watch']);