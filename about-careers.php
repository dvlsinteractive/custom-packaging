<?php 
	$subnav = true;
	$page = "careers";
	$section = "about";
	
	include('header.php'); 
?>

<section class="twoColumnText content-last">
	<div class="container">
		<h1>Career <strong>Opportunities</strong></h1>
		<aside>
			<p>We currently have openings for:</p>
			<ul>
				<li>Sales Representative</li>
			</ul>
			
			<div class="singleImage">
				<img src="https://unsplash.it/540/330/?random" />
			</div>
		</aside>
		
		<div class="content">
			<form>
				<span class="half">
					<label>Name:</label>
					<input type="text" />
				</span>
				
				<span class="half">
					<label>Email Address:</label>
					<input type="text" />
				</span>
				
				<span class="half">
					<label>Phone:</label>
					<input type="text" />
				</span>
				
				<span class="half">
					<label>Address:</label>
					<input type="text" />
				</span>
				
				<span class="full">
					<label>Upload Your Resume:</label>
					<input type="file" />
				</span>
				
				<span class="full">
					<label>Position(s) Applied for:</label>
					<textarea></textarea>
				</span>
				
				<span class="full">
					<button type="submit">Submit</button>
				</span>
			</form>
		</div>
	</div>
</section>




<?php include('footer.php'); ?>