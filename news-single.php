<?php 
	$subnav = false;
	$page = "news";
	$section = "news";
	
	include('header.php'); 
?>

<section class="news">
	<div class="container">
		<div class="newsText group">
			<h1>News Title Here</h1>
			<span class="publishDate">June 30, 2017</span>
			<p>The machine scheduled to be installed is a Bobst 1636-NT.  It will provide us with the ability to run up to 145 inch wide sheets of corrugated through a start-to-finish box making process. By start-to-finish, that means it will print, slot, die cut, glue, fold, bundle, stack, and discharge finished bales.  It will run 10,000 sheets per hour, meaning it will have the capacity to make 167 finished units (boxes) per minute, or put out one fill bale per minute.  The amazing part is the size of the product it can run at those speeds!</p>
			
			<img src="webimages/cardboard.jpg" class="alignright" />

			<p>The new machine is scheduled to arrive in November. We are looking forward to seeing the completion of this major project!</p>
		</div>
		
		<a href="news.php" class="newsBackBtn"><span class="icon-chevron-left"></span>Back To News</a>	
	</div>
</section>

<div class="newsBackground" style="background:url('webimages/cp-exterior.jpg');"></div>

<?php include('footer.php'); ?>