<?php 
	$subnav = true;
	$page = "management";
	$section = "about";
	
	include('header.php'); 
?>

<section class="management">
	<div class="container">
		<h1>Our <strong>Management</strong></h1>
		<div class="featuredImage">
			<img src="http://placehold.it/1200x600" />
		</div>
		
		<div class="managementThumbs">
			<div class="thumbnail">
				<img src="http://placehold.it/380x430" />
				<p>Gary West</p>
				<p>President</p>
			</div>
			
			<div class="thumbnail">
				<img src="http://placehold.it/380x430" />
				<p>Julie West</p>
				<p>Vice President</p>
			</div>
			
			<div class="thumbnail">
				<img src="http://placehold.it/380x430" />
				<p>Greg Butler</p>
				<p>CFO</p>
			</div>
			
			<div class="thumbnail">
				<img src="http://placehold.it/380x430" />
				<p>Dave Hennessey</p>
				<p>GM Arden, NC</p>
			</div>
			
			<div class="thumbnail">
				<img src="http://placehold.it/380x430" />
				<p>Tom Hancock</p>
				<p>Plant Manager, NC</p>
			</div>
		</div>
	</div>
</section>




<?php include('footer.php'); ?>