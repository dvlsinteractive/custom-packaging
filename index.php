<?php 
	$page = 'home';
	include('header.php');
?>

<section class="homeIntro fullWidth">
	<div class="container">
		<div class="introText">
			<a href="#" title="Custom Packaging" class="logo">
				<?php include('includes/logo.php'); ?>
			</a>
			
			<span class="innerText">
				<p>Custom Packaging, LP., a leader in the design and manufacturing of corrugated displays, graphics and industrial packaging, has been growing, innovating and creating practical solutions for manufacturers, retailers, and their customers worldwide since 1968.</p>
			
				<a href="#_" class="btn">View Our Products</a>
			</span>
			
			<div class="carousel-nav-home">
				<a href="#_" class="prev"><span class="icon-chevron-left"></span> <span class="linkText">prev</span></a>
				<a href="#_" class="next"><span class="linkText">next</span><span class="icon-chevron-right"></span></a>
			</div>
		</div>
		
		<div class="carousel">
			<ul class="carousel-slides auto">
				<li style="background:url('webimages/dermstore.jpg');"></li>
				<li style="background:url('webimages/bowlingBoxes.jpg');"></li>
				<li style="background:url('webimages/flwry.jpg');"></li>
			</ul>	
		</div>
		
		<div class="navBar">
			<ul class="primary">
				<li><a href="products.php">Products</a></li>
				<li><a href="services.php">Services</a></li>
				<li><a href="sustainability.php">Sustainability</a></li>
				<li><a href="about.php">About Us</a></li>
				<li><a href="contact.php">Contact</a></li>
	    </ul>
	    
	    <ul class="secondary">
				<li><a href="news.php">News</a></li>
				<li class="social"><a href="#"><span class="icon-twitter"></span></a></li>
				<li class="social"><a href="#"><span class="icon-instagram"></span></a></li>
			</ul>
		</div>
	
	</div>
</section>


<section class="blockLinks animatedParent animateOnce" data-appear-top-offset="-250" data-sequence="300">
		<a href="#_" class="animated fadeInLeft" data-id="1">
			<img src="webimages/SnapOnBoots-cropped.jpg" alt="Snap-On and Carhart packaging examples">
			<span>Our <strong>Products</strong></span>
		</a>
		
		<a href="#_" class="animated fadeIn" data-id="2">
			<img src="webimages/printingMachine-cropped.jpg" alt="Packaging being printed at Custom Packaging">
			<span>Our <strong>Services</strong></span>
		</a>
		
		<a href="#_" class="animated fadeInRight" data-id="1">
			<img src="webimages/recycle-cropped.jpg" alt="recycled cardboard at Custom Packaging">
			<span>Our <strong>Sustainability</strong></span>
		</a>
		
		<div class="group"></div>
</section>


<section class="about fullWidth">
	<div class="container">
		<div class="bgImage" style="background:url('webimages/homepage-about.jpg');"></div>
		<div class="text">
			<h2 id="pageTitle">About <br>Us</h2>
			<p>Custom Packaging, Inc. is a family-owned and operated independent sheet plant.  Since 1968, we have been a leader in the manufacturing of corrugated cartons, graphics packaging and point-of-purchase displays.  With continual investment in our equipment, technology and employees, Custom Packaging has built a company with the ability and expertise to help you throughout the packaging process. From concept to completion, we can provide valuable time-saving service to your company.</p>
			
			<a href="#_" class="btn">Find Out More</a>
		</div>
	</div>
</section>


<section class="locationInfo fullWidth">
	<div class="container">
		<div class="image">
			<img src="webimages/CustomPackagingExterior.jpg" alt="Custom Packaging Facility, Lebanon, TN">
		</div>
		
		<div class="text">
			<span itemscope="" itemtype="http://schema.org/Organization" class="spaceBelow">
				<span itemprop="name" class="screenreadable">Custom Packaging Lebanon, TN</span>
				<div class="address">
					<p itemprop="streetAddress">1315 West Baddour Parkway</p>
					<p><span itemprop="addressLocality">Lebanon, TN</span> <span itemprop="postalCode">37087</span></p>
				</div>
				<div class="phone">
					<p>Phone: <span itemprop="telephone">(615) 444-6025</span></p>	
					<p>Fax: <span itemprop="telephone">(615) 444-6024</span></p>
				</div>
			</span>
		
			<span itemscope="" itemtype="http://schema.org/Organization">
				<span itemprop="name" class="screenreadable">Custom Packaging Arden, NC</span>
				<div class="address">
					<p itemprop="streetAddress">20 Beale Road</p>
					<p><span itemprop="addressLocality">Arden, NC</span> <span itemprop="postalCode">28704</span></p>
				</div>
				<div class="phone">
					<p>Phone: <span itemprop="telephone">(828) 684-5060</span></p>	
					<p>Fax: <span itemprop="telephone">(828) 684-5037</span></p>
				</div>
			</span>
		</div>		
	</div>
</section>



<?php include('footer.php'); ?>