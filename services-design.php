<?php 
	$subnav = true;
	$page = "design";
	$section = "services";
	
	include('header.php'); 
?>

<section class="fullWidth">
	<div class="container">
		
		<div class="carousel">
			<ul class="carousel-slides auto">
				<li><img src="http://placehold.it/1400x550" /></li>
				<li><img src="http://placehold.it/1400x550" /></li>
				<li><img src="http://placehold.it/1400x550" /></li>
			</ul>	
			
			<div class="carousel-nav">
				<a href="#_" class="prev"><span class="icon-chevron-left"></span> <span class="linkText">prev</span></a>
				<a href="#_" class="next"><span class="linkText">next</span><span class="icon-chevron-right"></span></a>
			</div>
		</div>
		
	</div>
</section>



<section class="twoColumnText content-last">
	<div class="container">
		
		
		<div class="content">	
			<h1>Content Right, Carousel Left</h1>	
			<p>The structural design of packaging must be both functional and economical.  Each member of Custom Packaging’s design team has on average 15 years of corrugated design experience. Our designers have backgrounds in P-O-P Displays as well as corrugated and industrial packaging.  Regardless of your industry, we can design and manufacture a custom solution to meet your needs.  Our design team has the creativity, knowledge and expertise to see your project through from concept to production. We work to maximize board efficiency and reduce waste and scrap in our design process. Custom Packaging is committed to sustainability, which begins with the design.</p>
			<p>Custom Packaging can produce renderings, as well as unprinted or full-color samples with our digital printing capability.</p>
			<p><a href="contact.php">Contact us</a> today to get an experienced and creative designer working on your project.</p>

			<ul>
				<li>10 full time on-site structural designers</li>
				<li>2 in-house graphics designers</li>
				<li>3 sample cutting tables for quick turnaround</li>
				<li>Large format digital UV printer for color mockups</li>
				<li>Artios CAD design software with 3-D capability</li>
				<li>Adobe graphics software</li>
				<li>Strada 3-D software for renderings</li>
				<li>Cape Palletizing Software</li>
			</ul>
			
		</div>
		
		<aside>	
			<div class="carousel popup-gallery">
				<div class="carousel-slides">
					<a href="http://placehold.it/850x600?text=slide+Lg">
						<span class="screen"></span>
						<img src="http://placehold.it/500x380/03A9F4/fff?text=slide+1">
					</a>
					
					<a href="http://placehold.it/850x600?text=slide+Lg">
						<span class="screen"></span>
						<img src="http://placehold.it/500x380/03A9F4/fff?text=slide+2">
					</a>
					
					<a href="http://placehold.it/850x600?text=slide+Lg">
						<span class="screen"></span>
						<img src="http://placehold.it/500x380/03A9F4/fff?text=slide+3">
					</a>
				</div>
				
				<div class="carousel-nav">
					<a href="#_" class="prev"><span class="icon-chevron-left"></span> <span class="linkText">prev</span></a>
					<a href="#_" class="next"><span class="linkText">next</span><span class="icon-chevron-right"></span></a>
					<div class="dots"></div>
				</div>
			</div>
			
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>
		<aside>
		
	</div>
</section>


<?php include('footer.php'); ?>