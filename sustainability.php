<?php 
	$subnav = false;
	$page = "sustainability";
	$section = "sustainability";
	
	include('header.php'); 
?>


<section class="sustainability">
	<div class="container">
		<h1>Sustainability</h1>
		
		<div class="links">			
			<div class="text">
				<a href="#">Corrugated- Economically Sustainable</a>
				<a href="#">Corrugated- Environmentally Sustainable</a>
				<a href="#">Corrugated- Socially Sustainable</a>
				<a href="#">Setting up a Packaging Recycling Program</a>
				<a href="#">What Can I Recycle</a>
				<a href="#">What happens to my Cardboard?</a>
				<a href="#">Paper Recycling</a>
				<a href="#">Reduction in wax usage</a>
			</div>
			
			<div class="image">
				<img src="webimages/cardboard.jpg" />
			</div>
			<div class="group"></div>
		</div>
				
		<div class="frame">	
			<h2>Renewable Energy Use</h2>	
			<iframe src="http://www.sunnyportal.com/Templates/PublicPage.aspx?page=ee796a34-d0fe-4d82-a7e2-60f9c8af0db7" width="100%" height="580"></iframe>
		</div>
				
	</div>
</section>


<?php include('footer.php'); ?>