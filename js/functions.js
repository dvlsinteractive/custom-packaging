$(document).ready(function() {

	var carouselElement = $('.carousel-slides');
	
	carouselElement.each(function(){
			
		var carouselOptions = {
			dots: true,
			infinite: true,
			speed: 500,
			adaptiveHeight: true,
			arrows: false,
			appendDots:$(this).next('.carousel-nav').find('.dots'),
			draggable: true,
			swipe: true,
			autoplaySpeed: 2500,
			pauseOnDotsHover: true,
			pauseOnHover: true
		}
		
		// if carosel has "auto" class
		if($(this).hasClass('auto')) {
			carouselOptions.autoplay = true
		} else {
			carouselOptions.autoplay = false
		}
		
		// initialize carousel with options
		$(this).slick(carouselOptions);
	});
	
	$('.carousel-nav').find('.prev').click(function(){
  	$(this).parent().prev(carouselElement).slick('slickPrev');
  	return false;
	});
	
	$('.carousel-nav').find('.next').click(function(){
  	$(this).parent().prev(carouselElement).slick('slickNext');
  	return false;
	});
	
	// adjustment for homepage carousel structure
	$('.carousel-nav-home').find('.prev').click(function(){
  	carouselElement.slick('slickPrev');
  	return false;
	});
	
	$('.carousel-nav-home').find('.next').click(function(){
  	carouselElement.slick('slickNext');
  	return false;
	});
	
	
	// lighbox
	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		fixedContentPos: false,
		gallery: {
		  enabled: true,
		  navigateByImgClick: true,
		  preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
		  tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		  titleSrc: function(item) {
		    return item.el.attr('title');
		  }
		}
	});

  $('.popup-video').magnificPopup({
    disableOn: 600,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false
  });
  
  
  $('.carousel-gallery').magnificPopup({
		delegate: 'a:not(.slick-cloned)',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		fixedContentPos: false,
		gallery: {
		  enabled: true,
		  navigateByImgClick: true,
		  preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
		  tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		  titleSrc: function(item) {
		    return item.el.attr('title');
		  }
		}
	});
	
  
  
  // mobile menu
  $('.hamburger').click(function(){
		$(this).toggleClass('is-active');
		$('.offCanvas').toggleClass('active');
		$('body').toggleClass('locked');
	});
  

  //product and services slider

  $('.galleryslider').slick({
	  dots: false,
	  arrows: true,
	  infinite: true, 

	  responsive: [
    {
      breakpoint: 1400,
      settings: {
        slidesToShow: 1,
	  		slidesToScroll: 1,
      }
    },
    {
      breakpoint: 2600,
      settings: {
        centerMode: true,
        slidesToShow: 2
      }
    }

    ]
	});

	
});